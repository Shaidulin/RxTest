var stream$ = Rx.Observable.create(function (observer) {
    console.log('Stream was created')
    observer.next('One');

    setTimeout(function () {
        observer.next('After 2 seconds');
    },2000);

    // setTimeout(function () {
    //     observer.complete();
    // },3000);

    // observer.complete();
    setTimeout(function () {
        observer.error('Wrong');
    },5000);

    observer.next('Two');
});

stream$.subscribe(function (data) {
         console.log('Subscribe:',data);
    },
    function (error) {
        console.log('error:',error);
    },
    function () {
        console.log('Completed');
    }
)